---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://www.wikidata.org/wiki/Wikidata:Events/Data_Quality_Days_2021
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-09-07 23:00:00.000000000 +01:00
    event_start_value2: 2021-09-14 23:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 815
layout: evento
title: 'Wikidata: Data Quality Days 2021'
created: 1630926269
date: 2021-09-06
---

