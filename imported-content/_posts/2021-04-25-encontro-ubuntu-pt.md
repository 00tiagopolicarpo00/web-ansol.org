---
categories: []
metadata:
  event_location:
  - event_location_value: Online
  event_site:
  - event_site_url: https://ubuntu-pt.org/evento/encontro-ubuntu-pt-you-say-pepino-i-say-cucumber-portuguese-translation-opportunities-for-ubuntu-mate-lancamento-hirsute-hippo/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2021-04-29 20:00:00.000000000 +01:00
    event_start_value2: 2021-04-29 20:00:00.000000000 +01:00
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 795
layout: evento
title: Encontro Ubuntu-pt
created: 1619375487
date: 2021-04-25
---
<p>2 vezes por ano (em Abril e Outubro), numa quinta-feira, a comunidade Ubuntu Portugal reúne-se para assinalar/festejar o lançamento de uma nova versão Ubuntu.</p><p>Para celebrar o lançamento da versão 21.04 do Ubuntu, que procura trazer o melhor do Software Livre ao público em geral.</p><p>Vem fazer o upgrade connosco!</p><p>Vamos ter também alguém muito especial que nos vai falar sobre/about:<br> One way to translate for Ubuntu is to translate for the flavours. This talk will highlight the translation opportunities for Ubuntu MATE, as well as some of the other Ubuntu flavours. For Ubuntu MATE, these opportunities involve translating the website, the Guide, and the Ubuntu MATE Welcome. We will talk about how to get started and share advice from other translators.</p><p>Who:<br> Monica Ayhens-Madon is a part of the Ubuntu MATE team, an Ubuntu Member, and part of the Community Team at Canonical. She enjoys using her previous education and experience as a historian and writing instructor to create documentation and processes to help bring communities together, welcome and engage new contributors, and accomplish amazing things. She and her husband live in the Atlanta metro area in the Southeastern United States with their increasing collection of monitors. In normal times, she loves exploring, whether it is in her city or across the world. Thankfully, she has a number of hobbies that she can do at home, including gaming, crafting, reading, cooking, and making a good cup of tea! She can be found on Twitter at https://twitter.com/communiteatime, Twitch at https://www.twitch.tv/communiteatime, and her blog is at https://www.communi-tea.io</p><p>O encontro irá decorrer pelas 21h do dia 29 de Abril e será feito em: https://meet.ubcasts.com/abr-2021.</p><p>Vemo-nos lá e traz um amigo! Todos são bem-vindos!</p>
