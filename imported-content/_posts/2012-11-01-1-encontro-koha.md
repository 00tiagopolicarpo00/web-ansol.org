---
excerpt: "<p><strong>1&ordm; Encontro Koha</strong>, um sistema integrado gest&atilde;o
  de bibliotecas em Software Livre, a decorrer no dia <strong>28 de Novembro na Escola
  Superior de Comunica&ccedil;&atilde;o Social do Instituto Polit&eacute;cnico de
  Lisboa</strong></p>\r\n"
categories: []
metadata:
  event_location:
  - event_location_value: Escola Superior de Comunicação Social - Instituto Politécnico
      de Lisboa
  event_start:
  - event_start_value: 2012-11-28 09:00:00.000000000 +00:00
    event_start_value2: 2012-11-28 17:15:00.000000000 +00:00
  node_id: 97
layout: evento
title: 1º ENCONTRO KOHA
created: 1351766125
date: 2012-11-01
---
<p class="rtecenter">Koha 3.8 &ndash; Sistema integrado de gest&atilde;o de bibliotecas em software livre</p>
<p>PARTE DA MANH&Atilde;</p>
<ul>
	<li>
		9h &ndash; Recep&ccedil;&atilde;o dos participantes</li>
	<li>
		10h &ndash; <span style="color:#ff0000;">Abertura</span><br />
		Presidente do Instituto Polit&eacute;cnico de Lisboa<br />
		Professor Doutor Vicente Ferreira<br />
		Presidente da Escola Superior de Comunica&ccedil;&atilde;o Social<br />
		Professor Doutor Jorge Ver&iacute;ssimo</li>
	<li>
		10h15 &ndash; <span style="color:#ff0000;">&ldquo;A escolha do software livre&rdquo;</span><br />
		Lu&iacute;sa Marques, bibliotec&aacute;ria da Escola Superior de Teatro e Cinema</li>
	<li>
		10h30 &ndash; <span style="color:#ff0000;">&ldquo;Quanto custa a implementa&ccedil;&atilde;o do software?&rdquo;</span><br />
		Rafael Ant&oacute;nio, consultor</li>
	<li>
		11h &ndash; Pausa para caf&eacute;</li>
	<li>
		11h15 &ndash; <span style="color:#ff0000;">&ldquo;A literacia da informa&ccedil;&atilde;o e sua rentabiliza&ccedil;&atilde;o no ensino superior&rdquo;</span><br />
		Maria da Luz Antunes, bibliotec&aacute;ria da Escola Superior de Tecnologia da Sa&uacute;de de Lisboa</li>
	<li>
		11h45 &ndash; <span style="color:#ff0000;">&ldquo;A implementa&ccedil;&atilde;o do KOHA&rdquo;</span><br />
		Lu&iacute;sa Marques, bibliotec&aacute;ria da Escola Superior de Teatro e Cinema</li>
	<li>
		12h15 &ndash; Espa&ccedil;o para quest&otilde;es</li>
	<li>
		12h30 &ndash; Intervalo para almo&ccedil;o</li>
</ul>
<p>PARTE DA TARDE &ndash; Explorando o KOHA 3.8: a nova vers&atilde;o</p>
<ul>
	<li>
		14h &ndash; <span style="color:#ff0000;">&ldquo;Arquitetura&rdquo;</span><br />
		&Acirc;ngelo Fonseca, bibliotec&aacute;rio da Escola Superior de Educa&ccedil;&atilde;o do Instituto Polit&eacute;cnico de Viseu</li>
	<li>
		14h15 &ndash; <span style="color:#ff0000;">&ldquo;Configura&ccedil;&atilde;o e administra&ccedil;&atilde;o&rdquo;</span><br />
		Lu&iacute;sa Marques, bibliotec&aacute;ria da Escola Superior de Teatro e Cinema</li>
	<li>
		14h30 &ndash; <span style="color:#ff0000;">&ldquo;Cataloga&ccedil;&atilde;o/Autoridades&rdquo;</span><br />
		Anabela Teixeira Lopes e Mafalda Andrade, bibliotec&aacute;rias da Escola Superior de Comunica&ccedil;&atilde;o Social</li>
	<li>
		14h45 &ndash; <span style="color:#ff0000;">&ldquo;Empr&eacute;stimos&rdquo;</span><br />
		Paula Carvalho, bibliotec&aacute;ria da Escola Superior de Tecnologia da Sa&uacute;de de Lisboa</li>
	<li>
		15h &ndash; <span style="color:#ff0000;">&ldquo;Relat&oacute;rios&rdquo;</span><br />
		Paula Carvalho, bibliotec&aacute;ria da Escola Superior de Tecnologia da Sa&uacute;de de Lisboa</li>
	<li>
		15h15 &ndash; <span style="color:#ff0000;">&ldquo;Ferramentas&rdquo;</span><br />
		L&iacute;lia Rodrigues, bibliotec&aacute;ria da Escola Superior de Dan&ccedil;a</li>
	<li>
		15h30 &ndash; Pausa para caf&eacute;</li>
	<li>
		15h45 &ndash;<br />
		<span style="color:#ff0000;">&ldquo;A vis&atilde;o do inform&aacute;tico: o caso de uma biblioteca p&uacute;blica&rdquo;</span><br />
		Jos&eacute; Anjos, inform&aacute;tico da C&acirc;mara Municipal de Albufeira<br />
		<span style="color:#ff0000;">&ldquo;A vis&atilde;o dos utilizadores: acessibilidade e autonomia&rdquo;</span><br />
		Manuel Moreno, bibliotec&aacute;rio da Faculdade de Letras, Universidade de Lisboa</li>
	<li>
		Debate</li>
	<li>
		17h15 &ndash; Encerramento</li>
</ul>
<p><span style="color:#ff0000;">Informa&ccedil;&otilde;es sobre inscri&ccedil;&otilde;es:</span></p>
<p class="rteindent1">A inscri&ccedil;&atilde;o na confer&ecirc;ncia &eacute; gratuita mas limitada &agrave; capacidade da sala. Inscreva-se atrav&eacute;s do <a href="https://docs.google.com/spreadsheet/viewform?fromEmail=true&amp;formkey=dGtqeFEwTlFTLWpkcDM0dzZjNEFId3c6MQ">formul&aacute;rio de Inscri&ccedil;&otilde;es</a> e confirme a inscri&ccedil;&atilde;o na <a href="https://docs.google.com/spreadsheet/pub?key=0AkuDM0Ahwt4sdGtqeFEwTlFTLWpkcDM0dzZjNEFId3c&amp;output=html">lista de inscritos</a></p>
<p class="rteindent1">N&atilde;o hesite em contactar-nos para qualquer esclarecimento: e.mail: <a href="mailto:encontrokoha@ipl.pt">encontrokoha@ipl.pt</a> tel. 214 989 400, tlm. 912380906</p>
<p>&nbsp;</p>
