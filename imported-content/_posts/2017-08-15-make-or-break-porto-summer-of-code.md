---
categories: []
metadata:
  event_location:
  - event_location_value: Porto
  event_site:
  - event_site_url: https://makeorbreak.portosummerofcode.com/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2017-09-07 23:00:00.000000000 +01:00
    event_start_value2: 2017-09-09 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 520
layout: evento
title: Make or Break - Porto Summer of Code
created: 1502816913
date: 2017-08-15
---

