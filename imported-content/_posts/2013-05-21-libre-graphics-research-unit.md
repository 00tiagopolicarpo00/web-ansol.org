---
categories: []
metadata:
  event_location:
  - event_location_value: Porto
  event_site:
  - event_site_url: http://lgru.net/archives/5415
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2013-05-21 23:00:00.000000000 +01:00
    event_start_value2: 2013-05-24 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 165
layout: evento
title: Libre Graphics Research Unit
created: 1369131824
date: 2013-05-21
---
<p>De 22 a 25 de maio, a Libre Graphics Research Unit reúne-se no Porto para o seu último encontro, a conclusão do projeto que faz agora dois anos.O encontro, organizado pela Manufactura Independente com o apoio da Constant, terá lugar nos Maus Hábitos.</p><p>A Libre Graphics Research Unit é uma unidade de investigação que reúne várias entidades culturais europeias com o objetivo de aprofundar as possíveis relações entre ferramentas criativas, práticas artísticas e cultura livre. Este encontro é o último numa série de vários que ocorreram em várias cidades pela Europa.</p><p>Ao longo destes dias, serão discutidos os vários trabalhos desenvolvidos no âmbito desta unidade de investigação: a *Piksels and Lines Orchestra*, a plataforma Superglue*, o protótipo *Graphical Shell*, o evento *Future Tools*, a estação de trabalho *Grafica Libre*, a iniciativa *Tools for a Read-Write World*, a publicação Considering your tools: a reader for designers and developers, entre outras.</p><p>Em paralelo, será inauguradauma exposição de livros produzidos com Software Livre e Open Source. No sábado, dia 25 de maio, teremos dois workshops dedicados à publicação, documentação e tipografia.</p><p>Este encontro final tem como objetivos estimular a discussão sobre os projetos desenvolvidos no contexto da Libre Graphics Research Unit, terminar a documentação do material produzido ao longo destes dois anos, refletir sobre os resultados e planear colaborações futuras.</p><p>A entrada nas iniciativas do evento é livre, aberta e gratuita.</p>
