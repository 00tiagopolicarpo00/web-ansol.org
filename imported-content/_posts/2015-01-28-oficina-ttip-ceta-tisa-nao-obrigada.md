---
categories: []
metadata:
  event_location:
  - event_location_value: GAIA (Alfama)
  event_site:
  - event_site_url: https://www.nao-ao-ttip.pt/oficina-ttipcetatisa-nao-obrigada-no-gaia/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-01-29 18:00:00.000000000 +00:00
    event_start_value2: 2015-01-29 18:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 274
layout: evento
title: OFICINA TTIP/CETA/TISA, Não, Obrigada!
created: 1422479029
date: 2015-01-28
---
<p><span style="font-size: large;"><strong>29 de Janeiro, </strong></span><strong><span style="font-size: large;">no espaço do G</span></strong><strong><span style="font-size: large;">AIA </span>em Alfama<br> </strong><a href="https://www.google.fr/maps/place/R.+da+Regueira+40,+1100-218+Lisboa,+Portugal/@38.7123779,-9.1294874,17z/data=%213m1%214b1%214m2%213m1%211s0xd1934767712409d:0xc27d2378f9470ab4" target="_blank">Rua da Regueira nº 40</a></p><p><strong><span style="font-size: large;">18 H &gt;</span> &nbsp;jantar popular</strong> “Come &amp; Cresce” com tema da “Satyagraha contra TTIP e acordos comerciais”<strong><br> </strong><strong>20 H<span style="font-size: large;"> &gt;</span> participa a oficina</strong><strong> ” TTIP/CETA/TISA, Não, Obrigada! ” </strong>Com a participação da Plataforma Não ao Tratado Transatlântico.</p>
