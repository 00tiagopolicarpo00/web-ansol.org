---
categories:
- linux
- meeting
- alemanha
metadata:
  event_location:
  - event_location_value: Wilhelm-Schickard-Institut für Informatik, Tübingen, Alemanha
  event_site:
  - event_site_url: http://www.tuebix.org
    event_site_title: Tübix
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-06-08 23:00:00.000000000 +01:00
    event_start_value2: 2018-06-08 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 242
  - tags_tid: 129
  - tags_tid: 260
  node_id: 586
layout: evento
title: TÜBIX
created: 1522871042
date: 2018-04-04
---
<p>Tübix could be something for you if you:<br><br>&nbsp;&nbsp;&nbsp; ... want to learn something from others<br>&nbsp;&nbsp;&nbsp; ... want to show or teach something to others<br>&nbsp;&nbsp;&nbsp; ... expert / professional / veteran / master<br>&nbsp;&nbsp;&nbsp; ... newbie / beginner / changer / prospective customer<br>&nbsp;&nbsp;&nbsp; ... by the way want to visit a computer museum<br>&nbsp;&nbsp;&nbsp; ... by the way want to see an 80cm telescope <br><br>Admission is free, but everyone helps a bit with .</p>
