---
categories: []
metadata:
  event_site:
  - event_site_url: https://smart.inovamedialab.org/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-01-27 00:00:00.000000000 +00:00
    event_start_value2: 2020-01-31 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 713
layout: evento
title: 'SMART Data Sprint | Digital Methods: theory-practice-critique'
created: 1573559392
date: 2019-11-12
---

