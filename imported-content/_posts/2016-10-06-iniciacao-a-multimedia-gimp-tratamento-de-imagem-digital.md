---
categories: []
metadata:
  event_location:
  - event_location_value: Biblioteca dos Coruchéus, Lisboa
  event_site:
  - event_site_url: http://blx.cm-lisboa.pt/noticias/detalhes.php?id=1122
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-10-08 09:30:00.000000000 +01:00
    event_start_value2: 2016-10-08 11:30:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 462
layout: evento
title: Iniciação à multimédia - GIMP (tratamento de imagem digital)
created: 1475748445
date: 2016-10-06
---

