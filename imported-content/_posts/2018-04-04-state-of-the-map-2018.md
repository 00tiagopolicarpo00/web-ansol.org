---
categories:
- openstreetmap
- cartografia
- floss
metadata:
  event_location:
  - event_location_value: Politecnico di Milano ‐ Piazza Leonardo da Vinci, Milan,
      Lombardy, Italy
  event_site:
  - event_site_url: https://2018.stateofthemap.org
    event_site_title: State of the Map 2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-07-27 23:00:00.000000000 +01:00
    event_start_value2: 2018-07-29 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 70
  - tags_tid: 280
  - tags_tid: 273
  node_id: 592
layout: evento
title: State of the Map 2018
created: 1522877584
date: 2018-04-04
---
<p>State of the Map 2018 is taking place from <strong>July 28th to 30th 2018</strong> in Milan, Italy. We are looking forward to designing a program with you - the OpenStreetMap community. Update: The call for session proposals has now closed and we are working hard to score and select the talks.</p><p>In the meantime, here is the outline of the program:</p><ul><li>July 28th: first main conference day with keynote speech, presentations, workshops and evening social event</li><li>July 29th: second main conference day with presentations, workshops and an academic track</li><li>July 30th: either a third day or join activities or an activity day with code, documentation sprints, mapping and workshops</li></ul><p>There will be plenty of breakout rooms throughout for discussions, code, documentation sprints, mapping and workshops. <a href="https://2018.stateofthemap.org/academictrack/">Academic talks</a> will be included this year and will most likely be between half a day and a full day on July 29th.</p>
