---
categories:
- software livre
- encontro
- julho
- frança
metadata:
  event_location:
  - event_location_value: University of Strasbourg, Strasbourg, France
  event_site:
  - event_site_url: https://2018.rmll.info/?lang=en
    event_site_title: Libre Software Meeting
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-07-06 23:00:00.000000000 +01:00
    event_start_value2: 2018-07-11 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 41
  - tags_tid: 74
  - tags_tid: 237
  - tags_tid: 238
  node_id: 564
layout: evento
title: Libre Software Meeting / Rencontres mondiales du logiciel libre
created: 1522844598
date: 2018-04-04
---
<p>The yearly Libre Software Meeting (LSM) is an opportunity for beginners as well as veteran technicians to meet during several days to promote, discuss, develop, use and interrogate the Libre technologies, tools and culture.</p><p>This year, the LSM will stand, for the second time since its creation, at the University of Strasbourg from July 7 to 12, 2018.</p><p>The goal of this call is to incite you to contribute, and to describe the organization of the meeting. Further details regarding the location and the agenda will come later. If you need any special additional information, do not hesitate to contact us by email at contact [at] listes2018.rmll.info.</p>
