---
categories:
- free software
- convention
- fsf
metadata:
  event_location:
  - event_location_value: Boston, USA
  event_site:
  - event_site_url: https://libreplanet.org/2016/
    event_site_title: LibrePlanet 2016
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-03-19 00:00:00.000000000 +00:00
    event_start_value2: 2016-03-20 00:00:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 122
  - tags_tid: 150
  - tags_tid: 151
  node_id: 377
layout: evento
title: LibrePlanet 2016
created: 1445894941
date: 2015-10-26
---
<p class="lead">LibrePlanet is an annual conference hosted by the&nbsp;<a href="https://www.fsf.org/">Free Software Foundation</a>&nbsp;for people who care about their digital freedoms, bringing together software developers, policy experts, activists, and computer users to learn skills, share accomplishments, and address challenges facing the free software movement. LibrePlanet 2016 will feature programming for a wide range of ages and experience levels.</p><p>Organized around the theme "Fork the System", the conference's sessions will examine how free software creates the opportunity of a new path for its users, allows developers to fight the restrictions of a system dominated by proprietary software by creating free replacements, and is the foundation of a philosophy of freedom, sharing, and change.</p><p>FSF members and students attend LibrePlanet at no cost!&nbsp;<a href="https://crm.fsf.org/join">Become a member now</a>.</p>
