---
categories: []
metadata:
  event_location:
  - event_location_value: Jitsi Meet
  event_site:
  - event_site_url: https://www.meetup.com/ubuntupt/events/272598251/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-08-20 20:00:00.000000000 +01:00
    event_start_value2: 2020-08-20 22:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 748
layout: evento
title: Encontro Ubuntu-pt
created: 1597788052
date: 2020-08-18
---

