---
categories: []
metadata:
  event_site:
  - event_site_url: https://www.meetup.com/devopsporto/events/256370094
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-11-27 00:00:00.000000000 +00:00
    event_start_value2: 2018-11-27 00:00:00.000000000 +00:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 634
layout: evento
title: 'DevOps Porto #22: Build Automation in a DevOps way!'
created: 1543074115
date: 2018-11-24
---

