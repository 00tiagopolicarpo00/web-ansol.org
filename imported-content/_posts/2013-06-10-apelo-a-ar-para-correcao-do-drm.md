---
categories:
- press release
- ansol
- drm
- imprensa
metadata:
  tags:
  - tags_tid: 9
  - tags_tid: 33
  - tags_tid: 10
  - tags_tid: 19
  node_id: 178
layout: article
title: Apelo à AR para correção do DRM
created: 1370894226
date: 2013-06-10
---
<p><img src="http://listas.ansol.org/pipermail/ansol-geral/attachments/20130610/1393b778/livro-preso-com-cadeado-0001.jpg" style="float: left;" height="134" width="150">Lisboa, 10 de Junho de 2013: A Associação Nacional para o Software Livre (ANSOL) apelou aos deputados da Assembleia da República que aprovem os projetos de lei 406/XII/2ª e 423/XII/2ª em discussão na próxima Quarta-feira dia 12 de Junho que essencialmente clarificam e corrigem determinados pontos relativos às Medidas Tecnólogicas Eficazes, que não funcionam da forma como codificadas no Código de Direito de Autor e Direitos Conexos, sem introduzir ou eliminar direitos atualmente codificados.<br><!--break--><br>"<em>Estes projetos não alteram os princípios gerais do DRM, apenas corrigem debilidades graves na lei</em>", afirma Rui Seabra, presidente da direção da ANSOL.<br><br>O apelo foi feito através do interface para contacto com os deputados existente no site do Parlamento, onde se explicam vários motivos para votar, apesar de isso ir contra os interesses do lobby das editoras livreiras, de fonogramas e videogramas:</p><ol><li>Violação da Diretiva 2001/29/CE que diz que o DRM não pode impedir as utilizações livres</li><li>A atual permissão legislativa concedida pela lei a privados para sobreporem os seus desejos ao codificado na lei</li><li>A sujeição de muitos cidadãos a um ano de cadeia para poderem usufruir das obras legitimamente adquiridas quando têm DRM</li><li>A republicação de obras no domínio público de forma sujeita a DRM, apesar de tal ser atualmente proibido</li><li>A publicação de obras criadas com fundos públicos é atualmente feita muitas vezes com DRM</li></ol><p>"<em>Seria incompreensível e democraticamente inexplicável que tais injustiças e autorizações legislativas ad-hoc continuem a existir na nossa lei</em>" conclui assim o apelo da ANSOL aos deputados, que ainda diz "<em>ter conhecimento de que alguns grupos parlamentares que estão a dar indicações aos seus deputados para votarem contra</em>".<br><br>Já na semana passada foi entregue a todos os grupos parlamentares uma carta da ANSOL com um livro trancado com um cadeado, mas com a chave do mesmo entregue junto bem como uma autorização para utilizar a chave para abrir, numa alusão à forma como o DRM funciona.<br><br>= LINKS =<br><br>0. O interface do Parlamento para contacto com os deputados<br><br></p><ul><li><a href="http://www.parlamento.pt/DeputadoGP/Paginas/Deputados.aspx">http://www.parlamento.pt/DeputadoGP/Paginas/Deputados.aspx</a></li><li><a href="https://ansol.org/politica/ar/deputados">https://ansol.org/politica/ar/deputados</a></li></ul><p>1. <a href="https://drm-pt.info/">Campanha da ANSOL contra o DRM</a><br><br></p>
