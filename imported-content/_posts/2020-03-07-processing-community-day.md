---
categories: []
metadata:
  event_location:
  - event_location_value: Coimbra
  event_site:
  - event_site_url: http://pcdcoimbra.dei.uc.pt
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2020-03-31 23:00:00.000000000 +01:00
    event_start_value2: 2020-03-31 23:00:00.000000000 +01:00
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 734
layout: evento
title: Processing Community Day
created: 1583595522
date: 2020-03-07
---

