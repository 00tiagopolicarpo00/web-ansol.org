---
categories: []
metadata:
  event_site:
  - event_site_url: http://problender.pt/conf2015/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-04-16 23:00:00.000000000 +01:00
    event_start_value2: 2015-04-16 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 306
layout: evento
title: Encontros BlenderPT 2015
created: 1428523206
date: 2015-04-08
---
<p><span lang="PT-BR">Encontros Blender PT 2015 é um evento que reúne investigadores, profissionais e freelancers no sentido de promover a ferramenta digital livre Blender, debater acerca do seu real impacto a nível académico e profissional e estabelecer pontes entre as mais diversas comunidades de produção 3D no âmbito da partilha de conhecimento, composto por palestras, workshops, coffee breaks e muitas outras supresas.</span></p>
