---
categories:
- gnome
- floss
- meeting
metadata:
  event_location:
  - event_location_value: Almería, Espanha
  event_site:
  - event_site_url: https://2018.guadec.org/
    event_site_title: Guadec 2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-07-05 23:00:00.000000000 +01:00
    event_start_value2: 2018-07-10 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 277
  - tags_tid: 273
  - tags_tid: 129
  node_id: 589
layout: evento
title: GUADEC 2018
created: 1522875777
date: 2018-04-04
---
<div class="container"><div class="row intro-home"><div class="col"><div class="h4 text-center">GUADEC brings together Free Software Enthusiasts and professionals from all over the world. Join us for six days of talks, demos, discussion, parties, games and more.</div></div></div><div class="row subintro-home"><div class="col"><p>Join us to find out about the latest technical developments, learn new skills and tools, attend talks by leading experts, and participate in workshops and discussions.</p><p>You will also get the opportunity to participate in the GNOME project, meet community members, as well as connect with local groups and organisations.</p></div></div></div>
