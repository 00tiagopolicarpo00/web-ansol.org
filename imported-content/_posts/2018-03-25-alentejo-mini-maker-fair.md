---
categories: []
metadata:
  event_location:
  - event_location_value: Arena de Évora, Évora
  event_site:
  - event_site_url: https://alentejo.makerfaire.com/
    event_site_title: Alentejo Mini Maker Fair
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-05-10 23:00:00.000000000 +01:00
    event_start_value2: 2018-05-11 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 559
layout: evento
title: Alentejo Mini Maker Fair
created: 1522018353
date: 2018-03-25
---
<p><br><br>A Maker Faire é um encontro de pessoas fascinantes e curiosas que gostam de aprender e adoram partilhar o que fazem. De engenheiros a artistas, de cientistas a artesãos, a Maker Faire um local para esses "makers" mostrarem os passatempos, experiências e projectos.<br><br>É o maior espetáculo de Mostra (e Conta) do planeta - uma demostração familiar de invenções, criatividade e engenho.<br><br>Veja o futuro e inspire-se.<br><br></p>
