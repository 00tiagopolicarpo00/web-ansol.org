---
categories: []
metadata:
  event_location:
  - event_location_value: ISCTE, Lisboa
  event_site:
  - event_site_url: http://moss.dcti.iscte.pt/?p=280
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-04-16 10:00:00.000000000 +01:00
    event_start_value2: 2016-04-16 12:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 412
layout: evento
title: 'MOSS - Aula Aberta: A Associação Nacional para o Software Livre'
created: 1460217412
date: 2016-04-09
---
<p>Marcos Marado, actual Presidente da Direcção da ANSOL, irá lecionar no próximo dia 16 de abril uma "Aula Aberta", dirigida aos alunos do Mestrado em Software de Código Aberto, mas aberta a todos os interessados, sobre a ANSOL e o Software Livre em Portugal.</p><p>A entrada é livre, no entanto agradecemos o <a href="http://moss.dcti.iscte.pt/?p=280">registo</a> por uma questão de logistica. Obrigado.</p>
