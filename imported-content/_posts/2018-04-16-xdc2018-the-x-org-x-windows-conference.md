---
categories:
- x-windows
- conferência
- galiza
- espanha
- free software
metadata:
  event_location:
  - event_location_value: Faculdade de Informática, UDC, Corunha, Galiza, Espanha
  event_site:
  - event_site_url: https://www.x.org/wiki/Events/XDC2018/
    event_site_title: XDC2018
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-09-25 23:00:00.000000000 +01:00
    event_start_value2: 2018-09-27 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 292
  - tags_tid: 293
  - tags_tid: 104
  - tags_tid: 269
  - tags_tid: 122
  node_id: 607
layout: evento
title: XDC2018 - The X.org X-Windows conference
created: 1523873569
date: 2018-04-16
---
<p><span>The 2018 X.Org Developer's Conference (XDC2018)</span> is being hosted by GPUL (Galician Linux User Group) in A Coruña, Spain, from the 26th through the 28th of September, 2018. The event will be held at Computer Science Faculty of University of A Coruña, Campus de Elviña, 15071, A Coruña, Galicia (Spain)</p><h2 id="livestream">Livestream</h2><p>TBD</p><h2 id="registration">Registration</h2><p>The conference is entirely free of charge. If you want to attend XDC2018 please add your name to the <a href="https://www.x.org/wiki/Events/XDC2018/Attendees/">attendees page</a>. Additionally please subscribe to the <a href="http://lists.x.org/mailman/listinfo/events">X.org events mailing list</a> where we may post updates.</p><h2 id="accomodation">Accomodation</h2><p>There are plenty of <a href="https://www.google.com/maps/d/viewer?ll=43.365794275165776%2C-8.396322510055711&amp;spn=0.088628%2C0.09922&amp;msa=0&amp;z=15&amp;mid=1HIxsUvvQ6C0tGyswFwxF-wSXNVI">hotels in the city</a> center for all budgets, we recommend to stay in a hotel in the city center as there are many options for dinner and for enjoying the night life of the city. There is public transport to go to the venue from the city center (<a href="http://www.tranviascoruna.com/en/lineas-y-horarios/?linea=udc">UDC line</a>).</p><h2 id="codeofconduct">Code of conduct</h2><p>XDC is meant to be a technical conference where everyone can enjoy themselves. To this end, we will ask you to be kind to each-others and check out our <a href="https://www.x.org/wiki/XorgFoundation/Policies/Harassment/">Anti-Harassment Policy</a>.</p><h2 id="meals">Meals</h2><p>There are several cafeterias at the campus. The organizers are working on closing a good lunch price for XDC attendees. WIP.</p><p>There is a shopping center (<a href="http://espaciocoruna.es/">Espacio Coruña</a>) nearby with several restaurants, including fast-food ones. It is just 1.0 km distance (<a href="https://www.openstreetmap.org/directions?engine=graphhopper_foot&amp;route=43.33288%2C-8.41153%3B43.33666%2C-8.41100#map=17/43.33478/-8.41183">12 minutes walk</a>).</p><h2 id="travel">Travel</h2><p>The nearest airport is A Coruña airport (LCG), which is connected to Madrid, Barcelona, Lisboa, London/Heathrow. To get to the venue using public transport from A Coruña Airport, use the Airport-City Center Bus line (Line 4051 Puerta Real - Aeropuerto), stopping at Bus Station (Estación de Autobuses), from there you can get a taxi or taking a bus nearby to the venue (<a href="http://www.tranviascoruna.com/en/lineas-y-horarios/?linea=udc">UDC bus line</a>).</p><p>Another near airport is Santiago de Compostela airport (SCQ), which is connected to Madrid, Barcelona, Brussels, Dublin, Frankfurt, Istanbul, Geneva, Basel/Mulhouse, Milan/Bergamo, Paris/CDG. It is about 50km from Coruña. It's a good option if you have a direct flight, but requires taking the bus from the airport to the Santiago train station and then take a train to Coruña.</p><p>There are also trains to A Coruña from Madrid and Barcelona. See <a href="http://www.renfe.com/EN/viajeros/index.html">renfe.com</a> website for more details about train tickets and schedules.</p><p>There are buses going to A Coruña from different cities of Spain, see <a href="https://www.alsa.com/en/web/bus/home">Alsa.com</a> for more details.</p><p>If you want more info about traveling to A Coruña, check this page (TBD).</p><h2 id="travelsponsoring">Travel Sponsoring</h2><p>If you are not on a corporate budget but have something to present, please contact the X.Org Foundation Board of Directors <a href="mailto:board@foundation.x.org">board@foundation.x.org</a> for travel sponsorship. (See <a href="https://www.x.org/wiki/Events/">Events</a> for details.)</p>
