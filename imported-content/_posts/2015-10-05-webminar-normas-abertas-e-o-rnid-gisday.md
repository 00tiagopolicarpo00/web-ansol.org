---
categories: []
metadata:
  event_location:
  - event_location_value: Web
  event_site:
  - event_site_url: https://sites.google.com/site/gisdayoaz2015/
    event_site_title: ''
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2015-11-18 10:00:00.000000000 +00:00
    event_start_value2: 2015-11-18 11:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  node_id: 347
layout: evento
title: 'Webminar: Normas Abertas e o RNID - GISDAY'
created: 1444083676
date: 2015-10-05
---
<p>O GISDAY 2015 vai ser celebrado em Portugal com um conjunto de Webminar's, incluindo um sobre Normas Abertas e o RNID, no qual Marcos Marado, presidente da Direcção da ANSOL, é um dos oradores convidados.</p><table style="border-collapse: collapse; border-color: #ffffff; border-width: 0px;" border="0" cellspacing="0"><tbody><tr><td style="width: 137px; height: 66px;"><span style="color: #999999;"><span style="background-color: transparent; font-size: medium;"><strong>10:00 - 11:30</strong></span></span></td><td style="width: 596px; height: 66px;"><span style="background-color: transparent; font-size: medium;">1º PAINEL</span><span style="font-size: medium;"><span style="line-height: 1.25; background-color: transparent;"> - </span>Seminário online </span><span style="font-size: medium; line-height: 1.25; background-color: transparent;">(<a href="https://sites.google.com/site/gisdayoaz2015/inscricoes/inscricoes-seminarios-online">inscrição</a>)<br> <strong>O Regulamento Nacional de Interoperabilidade Digital e a adoção de Normas Abertas pela Administração Pública</strong></span></td></tr><tr><td style="width: 137px; height: 16px;"><span style="font-size: medium;">&nbsp;</span></td><td style="width: 596px; height: 16px;"><span style="font-size: medium;"><span style="line-height: 107%; font-family: Calibri,sans-serif;"><strong>Oradores:</strong><br>Marcos Marado – ANSOL<br>Maria João Marques – AMA <br>Ricardo Pinho – CMOA<br><strong>Debate</strong></span></span></td></tr></tbody></table>
