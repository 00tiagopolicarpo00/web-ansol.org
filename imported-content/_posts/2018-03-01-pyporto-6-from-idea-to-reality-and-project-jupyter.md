---
categories:
- python
- porto
metadata:
  event_location:
  - event_location_value: Founders Founders
  event_site:
  - event_site_url: https://www.meetup.com/pyporto/events/247610018/?rv=ea1&_xtd=gatlbWFpbF9jbGlja9oAJDc5NmUwODBiLTk3ZTgtNDQ0Zi05YWYyLTgyYjcxNDJmMTY4NQ
    event_site_title: 'PyPorto #6: From idea to reality & Project Jupyter'
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2018-03-07 18:30:00.000000000 +00:00
    event_start_value2: 2018-03-07 20:30:00.000000000 +00:00
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 207
  - tags_tid: 142
  node_id: 546
layout: evento
title: 'PyPorto #6: From idea to reality & Project Jupyter'
created: 1519929084
date: 2018-03-01
---
<p>We are happy to announce the 6th meetup! :)<br><br>This time we'll be at Founders Founders (right next to the metro Marques) bringing fresh new Pythonic talks.<br>Drinks and food will be kindly provided by Tonic App.<br><br>## Schedule<br><br>We are trying out a new format for our meetups:<br>- 18h30: Meeting and drinking :)<br>- 18h45: Creating a powerful tool with Python. Our success story by Tomás Lima<br>- 19h10: Beers and Chats<br>- 19h40: Project Jupyter. More that just a Pyton Notebook by Roman<br>- 20h10: End :(<br><br>## Creating a powerful tool with Python. Our success story<br><br>Three years ago a group of passionate Information Security professionals set to themselves an ambitious goal to create an enterprise-grade solution for IT security teams. They picked Python as a platform, despite having close to zero experience with it in the beginning. The idea is turned eventually to an Open Source project, which is actively maintained by the community and adopted by several organizations around the world. In the talk Tomás will outline his motivation, challenges, lessons learned and answer the questions of why we decided to use Python and why is important to be open source.<br><br>Tomás Lima is a Information Security professional with background in development.<br><br>## Project Jupyter. More that just a Pyton Notebook<br><br>Formerly known as IPython, the project Jupyter demonstrates a phenomenal growth. Now it’s literally everywhere, used from tutoring to data analysis, and from a conference and meetup presentation platform to a website engine. With more than hundred kernels, it allows you to write and execute the code not just in Python, but in R, Julia, MATLAB… and then Perl, PHP, Erlang, bash… and in the languages you’ve probably never heard of. Can you imagine? it’s 100+ literally! I didn’t know that.<br><br>A couple of weeks ago the project released the first stable version of the JupyterHub, a pluggable installation of the Jupyter platform suitable for collaborative work. This news gave me a pretext to talk about Jupyter and what kind of tool it can be turned into, for you and probably for the organization you work on.<br><br>I start with a bit of the IPython and Jupyter history and architecture, then make an overview of their newer, much more powerful but less known projects JupyterLab and JupyterHub, and finish my talk some speculations on how it all can be exploited for your fun and profit.<br><br>Roman is the head of the web development team of Doist, the software company which helps people do more and stress less. Calling himself climatic refugee from Russia, almost three years ago he moved with his family to Porto, and fell in love with the city and people. Along with other likeminded persons, he fosters and grows the Python community of the city.</p>
