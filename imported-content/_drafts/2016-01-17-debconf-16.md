---
categories: []
metadata:
  event_location:
  - event_location_value: University of Cape Town Breakwater Campus, Cape Town, South
      Africa
  event_site:
  - event_site_url: http://debconf16.debconf.org/
    event_site_title: Debconf 16
    event_site_attributes: a:0:{}
  event_start:
  - event_start_value: 2016-06-22 23:00:00.000000000 +01:00
    event_start_value2: 2016-06-30 23:00:00.000000000 +01:00
  slide:
  - slide_value: 0
  node_id: 389
layout: evento
title: Debconf 16
created: 1453065049
date: 2016-01-17
---
<h2>Advancement of Debian</h2><p>With more than 350 participants projected, we expect that the first DebConf in Africa will provide an intense working environment, and will lead to major advances for Debian and for Free Software in general.</p><p>We invite you to join and support this effort!</p><p>&nbsp;</p><p>https://wiki.debconf.org/wiki/DebConf16</p>
