---
categories:
- consultoria
- formação
- suporte
metadata:
  email:
  - email_email: apoio.clientes@crc-pt.com
  servicos:
  - servicos_tid: 7
  - servicos_tid: 3
  - servicos_tid: 2
  site:
  - site_url: http://crc-pt.com
    site_title: http://crc-pt.com
    site_attributes: a:0:{}
  node_id: 39
layout: servicos
title: CRC CONSULTORES
created: 1334499112
date: 2012-04-15
aliases:
- "/node/39/"
- "/servicos/39/"
---

