---
categories:
- consultoria
- desenvolvimento
- suporte
metadata:
  servicos:
  - servicos_tid: 7
  - servicos_tid: 5
  - servicos_tid: 2
  site:
  - site_url: http://www.valtex-tec.com/
    site_title: ''
    site_attributes: a:0:{}
  node_id: 427
layout: servicos
title: Valtex-tec
created: 1465226285
date: 2016-06-06
aliases:
- "/node/427/"
- "/servicos/427/"
---
<p>Empresa que implementa e desenvolve soluções de Software Livre, que presta serviços de consultoria, desenvolvimento, implementação e suporte de soluções.</p>
