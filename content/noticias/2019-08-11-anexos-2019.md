---
categories: []
metadata:
  anexos:
  - anexos_fid: 54
    anexos_display: 1
    anexos_description: logo ANSOL variante N
    anexos_uri: "/attachments/ANSOL_N.png"
  - anexos_fid: 55
    anexos_display: 1
    anexos_description: Live Software Livre
    anexos_uri: "/attachments/2019-09-17-livestreaming-cover-marcos-marado.png"
  - anexos_fid: 56
    anexos_display: 1
    anexos_description: Autores Portugueses em Domínio Público - Classe de 2020
    anexos_uri: "/attachments/montagem.jpg.png"
  - anexos_fid: 57
    anexos_display: 1
    anexos_description: imagem do site do jitsi
    anexos_uri: "/attachments/jitsi.png"
  - anexos_fid: 66
    anexos_display: 1
    anexos_description: Uvas e Pêssegos de Albino Armando no MU.SA - Museu das Artes
      de Sintra http://museuvirtual.cm-sintra.pt/sala-11/obra-2
    anexos_uri: "/attachments/pd2021.jpg"
  - anexos_fid: 68
    anexos_display: 1
    anexos_description: Imagem da apresentação feita pela ANSOL no evento da ANPRI
      - 2021
    anexos_uri: "/attachments/ANSOL-ANPRI.png"
  - anexos_fid: 69
    anexos_display: 1
    anexos_description: Notícia sobre o Porto Cidade Tecnológica 2001, incluindo o
      lançamento da ANSOL
    anexos_uri: "/attachments/JUP-Nov2001.pdf"
  - anexos_fid: 70
    anexos_display: 1
    anexos_description: À Conquista do Mundo, Hugo Séneca, Exame Informática, nº 90,
      Dezembro de 2002, pág. 84
    anexos_uri: "/attachments/Exame_Informatica-Dez2002.pdf"
  - anexos_fid: 71
    anexos_display: 1
    anexos_description: "  Permitido Copiar, Ana Correia Moutinho, Visão, nº 479,
      9 a 15 de Maio de 2002, pág. 104."
    anexos_uri: "/attachments/Visao-Maio2002.pdf"
  - anexos_fid: 72
    anexos_display: 1
    anexos_description: ANSOL - GLUA
    anexos_uri: "/attachments/ansol.png"
  slide:
  - slide_value: 0
  node_id: 688
layout: page
title: Anexos 2019
created: 1565539674
date: 2019-08-11
aliases:
- "/node/688/"
- "/page/688/"
---
<p>Página com vários anexos úteis sobre ou para a ANSOL - versão 2019.</p><p>Ver também: <a href="https://ansol.org/node/484">Anexos 2017</a>.</p>
