---
categories: []
metadata:
  anexos:
  - anexos_fid: 27
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/ilovefs-heart-px.png"
  - anexos_fid: 28
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/ilovefs-hashtag.png"
  - anexos_fid: 29
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/efd.png"
  - anexos_fid: 30
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/cyngnpt.jpg"
  - anexos_fid: 31
    anexos_display: 1
    anexos_description: Evento Direitos de Autor e DRM, 2015
    anexos_uri: "/attachments/all.jpg"
  - anexos_fid: 32
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/Ata 8 - ANSOL - Assembleia Geral.pdf"
  - anexos_fid: 36
    anexos_display: 1
    anexos_description: SFD 2015
    anexos_uri: "/attachments/IMG_20150919_134644.jpg"
  - anexos_fid: 37
    anexos_display: 1
    anexos_description: Protesto DRM W3C
    anexos_uri: "/attachments/Protest_Exterior_03.medium.png"
  - anexos_fid: 38
    anexos_display: 1
    anexos_description: Poster do evento do RMS em 2016 (ISCTE)
    anexos_uri: "/attachments/RMS2016.jpg"
  - anexos_fid: 39
    anexos_display: 1
    anexos_description: ''
    anexos_uri: "/attachments/ANSOL-Horizonte2020.pdf"
  slide:
  - slide_value: 0
  node_id: 278
layout: page
title: Anexos
created: 1422829091
date: 2015-02-01
aliases:
- "/node/278/"
- "/page/278/"
---
<p>Página de anexos a serem mostrados no site.</p>
