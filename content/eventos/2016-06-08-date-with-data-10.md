---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 428
  event:
    location: Pavilhão Jardim do UPTEC PINC, Porto
    site:
      title: ''
      url: http://datewithdata.pt/
    date:
      start: 2016-07-09 10:00:00.000000000 +01:00
      finish: 2016-07-09 17:00:00.000000000 +01:00
    map: {}
layout: evento
title: 'Date With Data #10'
created: 1465403773
date: 2016-06-08
aliases:
- "/evento/428/"
- "/node/428/"
---
<p>Sábado dia 9 há mais Date With Data!<br> <br> Antes de partirmos para o verão, vamos arrumar a casa e olhar para o que fizemos este ano! Junta-te a nós para pormos as mãos na massa, arrumarmos projectos quase terminados&nbsp;e&nbsp;marinarmos ideias antes de voltar à carga, em setembro.<br> <br> Como de costume, andaremos à volta de datasets para fazer crescer a nossa <a href="http://transparenciahackday.us6.list-manage.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=1cde43dd90&amp;e=b684bb14e9" target="_blank" style="color: #2baadf; font-weight: normal; text-decoration: underline;" data-saferedirecturl="https://www.google.com/url?q=http://transparenciahackday.us6.list-manage.com/track/click?u%3D7c39a9eb2d8160fd5975e6fec%26id%3D1cde43dd90%26e%3Db684bb14e9&amp;source=gmail&amp;ust=1467743866359000&amp;usg=AFQjCNF5pLtl6ID-9P5QrLkDDkRLk4Ji0w">Central de Dados</a>, mas há mais!&nbsp;<br> <br> A transcrição do&nbsp;<strong>English as She is Spoke</strong>&nbsp;ainda tem pano para mangas (o livro foi esta semana destacado no <a href="http://transparenciahackday.us6.list-manage.com/track/click?u=7c39a9eb2d8160fd5975e6fec&amp;id=58cf938c39&amp;e=b684bb14e9" target="_blank" style="color: #2baadf; font-weight: normal; text-decoration: underline;" data-saferedirecturl="https://www.google.com/url?q=http://transparenciahackday.us6.list-manage.com/track/click?u%3D7c39a9eb2d8160fd5975e6fec%26id%3D58cf938c39%26e%3Db684bb14e9&amp;source=gmail&amp;ust=1467743866359000&amp;usg=AFQjCNEYA1_qQUHjFI4kevhm_5WGFDN8Jw">Atlas Obscura</a>; temos&nbsp;<strong>traduções</strong><strong>&nbsp;de textos</strong>&nbsp;prontas para revisão antes de as publicarmos;&nbsp;<strong>datasets</strong>&nbsp;à espera de ideias e de atenção; e mais alguns projetos, a cozinhar em lume lento.&nbsp;É o pretexto ideal para aparecer pela primeira vez e ver de perto o que se faz nos Date With Data -- e um bom momento para sujar as mãos com dados, sites, hacks e apps!<br> <br> Voltamos a encontrar-nos no&nbsp;<strong>Pavilhão-Jardim</strong>&nbsp;do Pólo de Indústrias Criativas (UPTEC PINC), no mesmo horário&nbsp;—&nbsp;das 10 às 17!&nbsp;Traz o teu portátil!</p>
