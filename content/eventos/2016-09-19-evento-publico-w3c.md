---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 453
  event:
    location: IST, Lisboa
    site:
      title: ''
      url: http://www.meetup.com/GeeksIn-Lisbon/events/233972259/?gj=co2&rv=co2
    date:
      start: 2016-09-19 19:00:00.000000000 +01:00
      finish: 2016-09-19 19:00:00.000000000 +01:00
    map: {}
layout: evento
title: Evento público W3C
created: 1474294513
date: 2016-09-19
aliases:
- "/evento/453/"
- "/node/453/"
---
<p>A W3C veio a Lisboa para um encontro a decorrer durante a semana.</p><p>Mas na segunda-feira, dia 19, terão também um evento público, para o qual a presença é aberta, sendo apenas necessário indicar a ida no site do evento.</p>
