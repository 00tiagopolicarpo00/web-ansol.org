---
categories: []
metadata:
  node_id: 92
  event:
    location: Lisboa
    site:
      title: 
      url: 
    date:
      start: 2011-04-25 00:00:00.000000000 +01:00
      finish: 2011-04-25 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Dia da Liberdade
created: 1350237580
date: 2012-10-14
aliases:
- "/evento/92/"
- "/node/92/"
---
<p>Desfile Comemorativo: 25 de Abril<br><br></p><p>A ANSOL esteve presente no desfile, a distribuir informação e celebrar a Liberdade também no Software.</p>
