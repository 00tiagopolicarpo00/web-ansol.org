---
layout: evento
title: "Dia da Internet 2022: Como Melhorar a Internet"
metadata:
  event:
    date:
      start: 2022-06-14 21:00:00
      finish: 2022-06-14 22:30:00
    location: Online
    site:
      url: https://diadainternet.pt
---

O "Dia da Internet" (Dia Mundial das Telecomunicações e da Sociedade da
Informação, tal como reconhecido pela ONU) celebra-se a 17 de Maio, e este ano
a ANSOL junta-se a várias outras comunidades para o celebrar.

Mais informação na [página do evento](https://diadainternet.pt).

Por motivos de saúde, este evento celebra-se um pouco depois do dia celebrado, a 14 de Junho.

## Oradores

Conversa moderada por João Ribeiro - Shifter.pt
 * André Barbosa - Wikimédia Portugal
 * Tiago Carrondo - ANSOL - Associação Nacional para o Software livre
 * Ricardo Lafuente - D3 - Defesa dos Direitos Digitais

## Mais informação

 * Data: 14 de Junho de 2022 - 21h00/22h30 (GMT+1) (o convívio pode continuar depois do encerramento do streaming)
 * Local: Online (via Jitsi), em [jitsi.diadainternet.pt](http://jitsi.diadainternet.pt) (por motivos de segurança o link para o local será atualizado algumas horas antes do evento, mas o endereço será igual e por isso podem guardar)
