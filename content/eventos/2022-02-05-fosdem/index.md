---
layout: evento
title: FOSDEM
metadata:
  event:
    location: Online
    site:
      url: https://fosdem.org/2022/
    date:
      start: 2022-02-05
      finish: 2022-02-06
---
FOSDEM is a free event for software developers to meet, share ideas and
collaborate.

Every year, thousands of developers of free and open source software from all
over the world gather at the event in Brussels.
