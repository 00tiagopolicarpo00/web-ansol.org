---
categories: []
metadata:
  node_id: 74
  event:
    location: 'Faculdade de Letras da Universidade do Porto  '
    site:
      title: ''
      url: http://portolinux.org/doku.php?id=encontrostecnicos:debian-translation-day
    date:
      start: 2012-06-16 00:00:00.000000000 +01:00
      finish: 2012-06-16 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Dia de Tradução de Debian
created: 1338382318
date: 2012-05-30
aliases:
- "/evento/74/"
- "/node/74/"
---
<p>&nbsp;</p>
<p>
	<meta content="text/html; charset=utf-8" http-equiv="content-type" />
</p>
<p style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; ">&nbsp;Porto Linux associa-se &agrave; comunidade&nbsp;<a class="urlextern" href="http://www.debianpt.org/" rel="nofollow" style="padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; color: rgb(111, 111, 111); text-decoration: none; background-image: url(http://portolinux.org/lib/tpl/minima/images/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; background-position: 0px 1px; background-repeat: no-repeat no-repeat; " title="http://www.debianpt.org/">Debian-PT</a>&nbsp;na organiza&ccedil;&atilde;o de uma&nbsp;<em style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; ">maratona</em>&nbsp;de tradu&ccedil;&otilde;es da distribui&ccedil;&atilde;o&nbsp;<a class="urlextern" href="http://www.debian.org/" rel="nofollow" style="padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; color: rgb(111, 111, 111); text-decoration: none; background-image: url(http://portolinux.org/lib/tpl/minima/images/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; background-position: 0px 1px; background-repeat: no-repeat no-repeat; " title="http://www.debian.org/">Debian</a>.</p>
<p style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; ">Este evento ter&aacute; lugar na&nbsp;<a class="urlextern" href="http://www.fl.up.pt/" rel="nofollow" style="padding-top: 1px; padding-right: 0px; padding-bottom: 1px; padding-left: 16px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; color: rgb(111, 111, 111); text-decoration: none; background-image: url(http://portolinux.org/lib/tpl/minima/images/link_icon.gif); background-attachment: initial; background-origin: initial; background-clip: initial; background-color: transparent; background-position: 0px 1px; background-repeat: no-repeat no-repeat; " title="http://www.fl.up.pt/">Faculdade de Letras da Universidade do Porto</a>, no dia&nbsp;<strong style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; ">16 de Junho de 2012</strong>, com in&iacute;cio marcado para as&nbsp;<strong style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; ">10h30m</strong>.</p>
<p style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 1em; margin-left: 0px; ">A parte da&nbsp;<strong style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; ">manh&atilde;</strong>&nbsp;ser&aacute; preenchida com apresenta&ccedil;&otilde;es e demonstra&ccedil;&otilde;es acerca do processo de tradu&ccedil;&atilde;o, boas pr&aacute;ticas, etc. e a parte da&nbsp;<strong style="padding-top: 0px; padding-right: 0px; padding-bottom: 0px; padding-left: 0px; margin-top: 0px; margin-right: 0px; margin-bottom: 0px; margin-left: 0px; ">tarde</strong>&nbsp;&eacute; dedicada &agrave; coloca&ccedil;&atilde;o de m&atilde;os na massa.</p>
