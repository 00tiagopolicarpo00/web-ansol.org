---
categories:
- software livre
- encontro
- polónia
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 41
  - tags_tid: 74
  - tags_tid: 253
  node_id: 573
  event:
    location: 'Poznan University of Technology Conference Center, ul. Piotrowo 3,
      Poznan, Poland '
    site:
      title: P.I.W.O.
      url: https://piwo.informatyka.org/
    date:
      start: 2018-04-28 00:00:00.000000000 +01:00
      finish: 2018-04-28 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: P.I.W.O. - XIII EDYCJA
created: 1522854687
date: 2018-04-04
aliases:
- "/evento/573/"
- "/node/573/"
---
<p><a href="http://piwo.informatyka.org" class="http">PIWO</a> is one of the most popular polish FLOSS events. For P.I.W.O organizers, it’s always been about mobilizing creative individuals and promoting innovative IT ideas, developed with open source philosophy. Since the first edition of P.I.W.O in 2004, the conference hosted many speakers associated with free software movement, among whom linux enthusiasts, hackers, web specialists and Open Hardware fans.</p>
