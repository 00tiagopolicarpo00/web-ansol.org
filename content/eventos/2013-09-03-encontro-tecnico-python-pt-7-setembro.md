---
categories:
- '2013'
metadata:
  slide:
  - slide_value: 0
  tags:
  - tags_tid: 32
  node_id: 205
  event:
    location: 'Fábrica de Santo Thyrso, Rua Dr. Oliveira Salazar, Nº 88, 4780-453
      Santo Tirso '
    site:
      title: ''
      url: http://python.pt/eventos/2013/09/07/encontro-tecnico-pythonpt-7-setembro/
    date:
      start: 2013-09-07 10:30:00.000000000 +01:00
      finish: 2013-09-07 17:00:00.000000000 +01:00
    map: {}
layout: evento
title: Encontro Técnico Python.pt 7 Setembro
created: 1378169967
date: 2013-09-03
aliases:
- "/evento/205/"
- "/node/205/"
---
<h2>Programa</h2><p>10:30 - Encontro na estação de São Bento (partida às 11:00) <br>12:00 - Tour às instalações da Fábrica de Santo Thyrso <br>13:00 - Almoço <br>14:00 - "Criação de Aplicações GUI com PyQt" - Rizo Isrof (@rizo_isrof) <br>15:00 - "Desenvolvimento Orientado a Testes em Django e Integração Contínua com Jenkins CI" - José Durães (@joseduraes) <br>16:00 - a confirmar </p>
