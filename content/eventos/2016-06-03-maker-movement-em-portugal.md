---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 426
  event:
    location: Aveiro
    site:
      title: ''
      url: https://hackaveiro.org/2016/06/european-maker-week-2016-programa/
    date:
      start: 2016-06-04 00:00:00.000000000 +01:00
      finish: 2016-06-04 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Maker movement em Portugal
created: 1464942642
date: 2016-06-03
aliases:
- "/evento/426/"
- "/node/426/"
---

