---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 167
  event:
    location: 'Escola Superior Agrária de Castelo Branco '
    site:
      title: ''
      url: http://sigencontro.esa.ipcb.pt/
    date:
      start: 2013-05-31 00:00:00.000000000 +01:00
      finish: 2013-05-31 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: IV Encontro de Sistemas de Informação Geográfica
created: 1369990346
date: 2013-05-31
aliases:
- "/evento/167/"
- "/node/167/"
---

