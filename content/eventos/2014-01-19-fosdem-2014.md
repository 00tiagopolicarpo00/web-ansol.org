---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 213
  event:
    location: ULB Solboch, Bruxelas, Bélgica
    site:
      title: FOSDEM
      url: https://fosdem.org/2014/
    date:
      start: 2014-01-31 18:00:00.000000000 +00:00
      finish: 2014-02-02 17:00:00.000000000 +00:00
    map: {}
layout: evento
title: FOSDEM 2014
created: 1390173993
date: 2014-01-19
aliases:
- "/evento/213/"
- "/node/213/"
---
<p>A maior conferência europeia anual de Software Livre feita por comunidades para as comunidades, habitualmente começa com o "<em>beer event</em>" no Delirium Café, Sexta-feira ao fim do dia, como forma de aquecer o ambiente antes dos dois dias intensos de conferências.</p>
