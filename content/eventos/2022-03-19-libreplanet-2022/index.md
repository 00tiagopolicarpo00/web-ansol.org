---
layout: evento
title: LibrePlanet 2022
metadata:
  event:
    location: Online
    site:
      url: https://libreplanet.org/2022/
    date:
      start: 2022-03-19
      finish: 2022-03-20
---

LibrePlanet is the annual conference hosted by the Free Software Foundation.
LibrePlanet provides an opportunity for community activists, domain experts,
and people seeking solutions for themselves to come together in order to
discuss current issues in technology and ethics.

This year's LibrePlanet theme is "Living Liberation". LibrePlanet speakers will
urge people to continue to make free software part of their daily lives, one
decision at a time. All of us communicate the importance of technological
freedom, both in terms of educating newcomers about the crucial implications of
the free software philosophy, and sustaining all those who contribute to the
liberation of technology.
