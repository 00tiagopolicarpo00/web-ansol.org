---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 388
  event:
    location: 
    site:
      title: ''
      url: https://fsfe.org/campaigns/ilovefs/2016/index.pt.html
    date:
      start: 2016-02-14 00:00:00.000000000 +00:00
      finish: 2016-02-14 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Eu <3 Software Livre 2016
created: 1452986677
date: 2016-01-16
aliases:
- "/evento/388/"
- "/ilovefs2016/"
- "/node/388/"
---
<img src="https://ansol.org/attachments/ilovefs-heart-px.png" alt="#ilovefs"
style="display: block; margin-left: auto; margin-right: auto; margin-bottom:
1em;">

Todos os anos a 14 de Fevereiro, a ANSOL junta-se à Free Software Foundation
Europe e pede a todos os utilizadores de Software Livre que pensem naquelas
pessoas trabalhadoras na comunidade de Software Livre e mostrem o seu apreço
individualmente neste dia de "Eu adoro o Software Livre".

Tal como no ano passado, a campanha é dedicada às pessoas por detrás do
Software Livre porque eles permitem-nos usar, estudar, partilhar e melhorar o
software que nos permite trabalhar em liberdade.
