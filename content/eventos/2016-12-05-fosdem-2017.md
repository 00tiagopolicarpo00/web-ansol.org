---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 474
  event:
    location: Bruxelas
    site:
      title: ''
      url: https://fosdem.org/2017/
    date:
      start: 2017-02-04 00:00:00.000000000 +00:00
      finish: 2017-02-05 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: FOSDEM 2017
created: 1480946512
date: 2016-12-05
aliases:
- "/evento/474/"
- "/node/474/"
---
<p>A ANSOL vai estar pela FOSDEM! Encontem-nos por lá (ou combinem connosco: marcos.marado@ansol.org )</p>
