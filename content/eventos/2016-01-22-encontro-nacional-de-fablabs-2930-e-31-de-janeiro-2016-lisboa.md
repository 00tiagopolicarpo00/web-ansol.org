---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 396
  event:
    location: " ISCTE, Lisboa"
    site:
      title: Programa do Encontro
      url: https://docs.google.com/spreadsheets/d/1L4WdBVuciIhXX3rtK-ayzfnQ6dFXF64Um9Y7HXPHthM/edit#gid=0
    date:
      start: 2016-01-29 00:00:00.000000000 +00:00
      finish: 2016-01-31 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Encontro Nacional de FabLabs /// 29,30 e 31 de Janeiro 2016, Lisboa
created: 1453493499
date: 2016-01-22
aliases:
- "/evento/396/"
- "/node/396/"
---
