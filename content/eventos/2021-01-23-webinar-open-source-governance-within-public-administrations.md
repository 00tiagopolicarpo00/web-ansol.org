---
categories: []
metadata:
  mapa:
  - {}
  slide:
  - slide_value: 0
  node_id: 771
  event:
    location: Online
    site:
      title: ''
      url: http://eolevent.eu/
    date:
      start: 2021-01-27 00:00:00.000000000 +00:00
      finish: 2021-01-27 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'Webinar: Open source governance within public administrations'
created: 1611431273
date: 2021-01-23
aliases:
- "/evento/771/"
- "/node/771/"
---

