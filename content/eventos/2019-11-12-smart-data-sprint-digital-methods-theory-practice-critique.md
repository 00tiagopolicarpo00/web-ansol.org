---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 713
  event:
    location: 
    site:
      title: ''
      url: https://smart.inovamedialab.org/
    date:
      start: 2020-01-27 00:00:00.000000000 +00:00
      finish: 2020-01-31 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: 'SMART Data Sprint | Digital Methods: theory-practice-critique'
created: 1573559392
date: 2019-11-12
aliases:
- "/evento/713/"
- "/node/713/"
---

