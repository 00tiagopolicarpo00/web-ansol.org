---
categories: []
metadata:
  mapa:
  - mapa_geom: "\x01\a\0\0\0\0\0\0\0"
    mapa_geo_type: geometrycollection
    mapa_geohash: ''
  slide:
  - slide_value: 0
  node_id: 633
  event:
    location: 
    site:
      title: ''
      url: https://www.meetup.com/Coimbra-JUG/events/256018061/?rv=ea1_v2&_xtd=gatlbWFpbF9jbGlja9oAJDJjYmQ0NWIwLTRjNWEtNDk3My1iYTFlLTgwMmNlMzFkMjg1Ng
    date:
      start: 2018-11-20 00:00:00.000000000 +00:00
      finish: 2018-11-20 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Hybrid compute for cloud Java & Introdução ao Deep Learning em Nuvem
created: 1542634884
date: 2018-11-19
aliases:
- "/evento/633/"
- "/node/633/"
---

