---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 311
  event:
    location: Parlamento
    site:
      title: 
      url: 
    date:
      start: 2015-05-08 00:00:00.000000000 +01:00
      finish: 2015-05-08 00:00:00.000000000 +01:00
    map: {}
layout: evento
title: Cópia Privada no Parlamento
created: 1429730952
date: 2015-04-22
aliases:
- "/evento/311/"
- "/node/311/"
---
<p>No dia 8 de Maio é discutida a reapreciação do decreto de lei da cópia privada, que foi vetado pelo Presidente da República, em simultâneo com a discussão da petição contra aquele diploma.</p>
