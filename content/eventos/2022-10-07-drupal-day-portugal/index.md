---
layout: evento
title: Drupal Day Portugal 2022
metadata:
  event:
    date:
      start: 2022-10-07
      finish: 2022-10-07
    location: NTT Data, Atrium Saldanha, Lisboa
    site:
      url: https://drupal.pt/drupal-day-portugal-2022
---


