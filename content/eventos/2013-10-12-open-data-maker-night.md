---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 210
  event:
    location: Liberdade 229, Lisboa
    site:
      title: ''
      url: http://flipside.org/notes/open-data-maker-night-II/
    date:
      start: 2013-10-17 18:30:00.000000000 +01:00
      finish: 2013-10-17 18:30:00.000000000 +01:00
    map: {}
layout: evento
title: Open Data Maker Night
created: 1381605669
date: 2013-10-12
aliases:
- "/evento/210/"
- "/node/210/"
---

