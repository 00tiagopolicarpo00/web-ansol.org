---
categories: []
metadata:
  slide:
  - slide_value: 0
  node_id: 259
  event:
    location: 
    site:
      title: ''
      url: http://www.europarl.europa.eu/committees/en/juri/home.html
    date:
      start: 2015-01-20 00:00:00.000000000 +00:00
      finish: 2015-01-20 00:00:00.000000000 +00:00
    map: {}
layout: evento
title: Presentation of draft report on Copyright before the Legal Affairs Committee
created: 1420196212
date: 2015-01-02
aliases:
- "/evento/259/"
- "/node/259/"
---

