---
categories: []
metadata:
  node_id: 99
layout: page
title: EUCD
created: 1351791606
date: 2012-11-01
aliases:
- "/node/99/"
- "/page/99/"
- "/politica/eucd/"
---
<p>O conte&uacute;do desta p&aacute;gina passou a ter apenas interesse hist&oacute;rico devido &agrave; publica&ccedil;&atilde;o da <a href="http://www.dre.pt/util/getpdf.asp?s=dip&amp;serie=1&amp;iddr=2004.199A&amp;iddip=20042861" title="Lei n.º 50/2004">Lei n.&ordm; 50/2004. <acronym title="Diário da República">D.R.</acronym> n.&ordm; 199, S&eacute;rie I-A de 2004-08-24</a>. Infelizmente uma boa parte dos problemas persistem.</p>
<p>A EUCD &eacute; uma directiva europeia que implementa, pela primeira vez, restri&ccedil;&otilde;es de acesso no Direito de Autor, para al&eacute;m de criminalizar quer a neutraliza&ccedil;&atilde;o de &quot;medidas eficazes de car&aacute;cter tecnol&oacute;gico&quot;, quer discuss&otilde;es que algu&eacute;m considere que possam facilitar a sua neutraliza&ccedil;&atilde;o. Infelizmente, pela sua reda&ccedil;&atilde;o a directiva interfere no desenvolvimento de Software Livre que inclua o que possa ser considerado como &quot;medidas eficazes de car&aacute;cter tecnol&oacute;gico&quot; (MECT) ao limitar a discuss&atilde;o, como acontece com o sistema de permiss&otilde;es do kernel Linux, e impede a cria&ccedil;&atilde;o de Software Livre que aceda a obras protegidas por este tipo de medidas, como software para leitura de DVDs.</p>
<p>Na altura, a <strong>proposta de lei 108/IX</strong>, esteve agendada para vota&ccedil;&atilde;o a <strong>25 de Fevereiro de 2004</strong>. Quaisquer propostas ou emendas teriam de ser apresentadas at&eacute; dia <strong>17 de Fevereiro de 2004</strong>. Preparamos uma proposta alternativa da implementa&ccedil;&atilde;o da directiva que tenta evitar o maior n&uacute;mero de problemas poss&iacute;veis. Este esfor&ccedil;o foi desenvolvido sobretudo pelo Jo&atilde;o Miguel Neves, ent&atilde;o presidente da dire&ccedil;&atilde;o da ANSOL.</p>
<h2>
	O que p&ocirc;de ser feito?</h2>
<h3>
	Divulga&ccedil;&atilde;o</h3>
<p>Em primeiro lugar, divulgar. Para isso reun&iacute;mos alguma informa&ccedil;&atilde;o dispon&iacute;vel nesta p&aacute;gina e no wiki em <span class="link-external"><a href="http://wiki.ansol.org/EucdCampanha">http://wiki.ansol.org/EucdCampanha</a></span>.</p>
<ul>
	<li>
		<a href="http://ate2012.ansol.org/politica/eucd/folheto-eucd.pdf" target="_self" title="Folheto EUCD (PDF)">Folheto A4 em PDF</a> - de um lado com informa&ccedil;&atilde;o sobre a proposta de lei 108/IX, do outro com situa&ccedil;&otilde;es onde a proposta de lei 108/IX muda a lei vigente. <a href="http://ate2012.ansol.org/politica/eucd/folheto-eucd.tex" target="_self" title="Folheto EUCD (TeX)">Vers&atilde;o edit&aacute;vel em LaTeX.</a></li>
	<li>
		<span class="link-external"><a href="http://www.ansol.org/ansolwiki/EucdTestesLegais">Testes Legais</a></span> - conjunto de situa&ccedil;&otilde;es que permitem avaliar o quanto uma pessoa concorda ou n&atilde;o com a legisla&ccedil;&atilde;o existente.</li>
	<li>
		<span class="link-external"><a href="http://www.ansol.org/ansolwiki/EucdPropostaLei">Breve an&aacute;lise da Proposta de Lei 108/IX</a></span></li>
	<li>
		<span class="link-external"><a href="http://www.ansol.org/ansolwiki/EucdPerguntasFrequentes">Perguntas Frequentes sobre a Proposta de Lei 108/IX</a></span></li>
</ul>
<h3>
	Contactar entidades e partidos pol&iacute;ticos</h3>
<p>Se tiver contactos entre entidades que possam sentir-se afectadas por esta legisla&ccedil;&atilde;o, passe-lhes a informa&ccedil;&atilde;o dispon&iacute;vel, em particular se forem partidos pol&iacute;ticos.</p>
<p><span class="link-external"><a href="http://www.parlamento.pt/Paginas/Contactos.aspx">Telefone, morada e email</a></span> dos grupos parlamentares.</p>
<h2>
	Casos</h2>
<ul>
	<li>
		<a href="http://ate2012.ansol.org/politica/eucd/ler-dvds.pt.html" target="_self" title="Ler DVDs de zonas diferentes">DVDs</a> - porque &eacute; que esta lei ilegaliza a leitura de DVDs que n&atilde;o seja a 2 (Europa) ou a 9 (sem restri&ccedil;&otilde;es).</li>
	<li>
		<a href="http://ate2012.ansol.org/politica/eucd/password.pt.html" target="_self" title="Perdi a minha password">Microsoft Office</a> - um exemplo de como o autor perde o exclusivo de definir o direito de acesso quando partilha medidas de car&aacute;cter tecnol&oacute;gico com outros.</li>
	<li>
		<a href="http://ate2012.ansol.org/politica/eucd/provas-tribunal.pt.html" target="_self" title="Fim de documentos como provas em tribunal">Fim de documentos como provas em tribunal</a> - a descri&ccedil;&atilde;o de um caso, at&eacute; agora virtual, de como esta lei pode p&ocirc;r em causa a justi&ccedil;a portuguesa.</li>
</ul>
<h2>
	Documenta&ccedil;&atilde;o</h2>
<ul>
	<li>
		Proposta de lei 108/IX - <a href="http://ate2012.ansol.org/politica/eucd/ppl108-IX.pdf" target="_self" title="Proposta de Lei 108/IX (PDF)">PDF</a>, <a href="http://ate2012.ansol.org/politica/eucd/ppl108-IX.html" target="_self" title="PROPOSTA DE LEI N.º 108/IX">HTML</a></li>
	<li>
		<a href="http://ate2012.ansol.org/politica/eucd/eucd-sl.pt.html" target="_self" title="DMCA e EUCD: Copyright vs Comunidade">DMCA e EUCD: Copyright vs Comunidade</a> - descri&ccedil;&atilde;o do impacto desta legisla&ccedil;&atilde;o com variados exemplos.</li>
	<li>
		<span class="link-external"><a href="http://www.fipr.org/copyright/guide/">Relat&oacute;rio de implementa&ccedil;&atilde;o da EUCD na Uni&atilde;o Europeia</a></span> (em ingl&ecirc;s)</li>
	<li>
		<span class="link-external"><a href="http://wiki.ael.be/index.php/EUCD-Status">Estado da implementa&ccedil;&atilde;o da EUCD na Uni&atilde;o Europeia</a></span> (em ingl&ecirc;s)</li>
</ul>
<h2>
	Documentos Hist&oacute;ricos</h2>
<ul>
	<li>
		<a href="http://ate2012.ansol.org/politica/eucd/eucd-texto.pt.html" target="_self" title="PROJECTO DE DECRETO LEI">Projecto-lei</a> publicado pelo Gabinete de Direito de Autor (3 de Dezembro de 2002).</li>
	<li>
		<a href="http://ate2012.ansol.org/politica/eucd/eucd-justificacao.pt.html" target="_self" title="NOTA JUSTIFICATIVA">Nota justificativa</a> publicada pelo Gabinete de Direito de Autor (3 de Dezembro de 2002).</li>
	<li>
		Proposta de altera&ccedil;&atilde;o do projecto-lei apresentada pela ANSOL (<a href="http://ate2012.ansol.org/politica/eucd/proposta-eucd-pt.ps" target="_self" title="Proposta EUCD (Postscript)">PS</a>)(<a href="http://ate2012.ansol.org/politica/eucd/proposta-eucd-pt.pdf" target="_self" title="Proposta EUCD (PDF)">PDF</a>)(<a href="http://ate2012.ansol.org/politica/eucd/eucd-pt.latex.zip" target="_self" title="Proposta EUCD (LaTeX, zip)">LaTeX</a>) (9 de Dezembro de 2002)</li>
</ul>
<p>Pode ainda consultar esta p&aacute;gina no site anterior (a maioria dos links reverte para l&aacute; de qualquer das formas): <a href="http://ate2012.ansol.org/politica/eucd/">http://ate2012.ansol.org/politica/eucd/</a></p>
